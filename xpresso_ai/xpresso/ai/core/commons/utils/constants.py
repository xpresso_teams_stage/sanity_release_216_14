""" Stores constant variable """

# General constants to be used anywhere
LIBRARY_XPRESSO_PACKAGE_NAME = "xpresso_ai"
FLASK_JSONIFY_SORT_KEYS = 'JSON_SORT_KEYS'
PASSWORD_ENCRYPTION_KEY = b"U9fBoMYPoEl_oyAZOZD3_JVQMAQJ1Ax6uNRBhqT9NIc="
FILE_ENCRYPTION_KEY = b"Wc50OE0NlKhY10wDLE32idq9Yv3oq95F92UDA9tefA8="
HTTP_OK = 200
permission_755 = 0o755
permission_765 = 0o765
permission_777 = 0o777
log_path_flag = 'log'
config_path_flag = 'config'
root_package_location = '/opt/xpresso.ai'
EMPTY_STRING = ""
CONFIG_DOCKER_REGISTRY = "docker_registry"
CONFIG_DOCKER_REGISTRY_HOST = "host"
CONFIG_DOCKER_REGISTRY_UID = "username"
CONFIG_DOCKER_REGISTRY_PWD = "password"
XPRESSO_ENV = "env"
XPRESSO_INSTANCE = "xpresso_instance"
HTTP_URL_PREFIX = "http://"
CREATED_BY_KEY = "created_by"
CREATED_ON_KEY = "created_on"
UPDATED_BY_KEY = "updated_by"
UPDATED_ON_KEY = "updated_on"
LAST_UPDATED_BY_KEY = "last_updated_by"
LAST_UPDATED_ON_KEY = "last_updated_on"
BLANK_DATA_INDICATOR = "-"

# Context
USER_KEY = "user"

# TODO Mount location should be taken from config
project_nfs_folder = "/mnt/nfs/data/projects"

database_component_type = "database"
component_deployment_manager_flavor = "component_deployment_manager_flavor"
secure_port_config = "secure_port"
MOUNT_PATH_KEY = 'mount_path'
CRON_SCHEDULE_KEY = 'cron_schedule'
REPLICAS_KEY = 'replicas'
CUSTOM_DOCKER_IMAGE_KEY = 'custom_docker_image'
BUILD_VERSION_KEY = 'build_version'
BUILD_DEPENDENCIES = "xpresso_dependencies"
BITBUCKET = 'bitbucket'
CURRENTLY_DEPLOYED_KEY = 'currentlyDeployed'
FLAVOR_KEY = 'flavor'
TYPE_KEY = 'type'
NAME_KEY = 'name'
COMPONENT_TYPE_SERVICE = 'service'
COMPONENT_TYPE_INFERENCE_SERVICE = 'inference_service'
COMPONENT_TYPE_JOB = 'job'
COMPONENT_TYPE_PIPELINE_JOB = 'pipeline_job'
COMPONENT_TYPE_CRON_JOB = 'cron_job'
COMPONENT_TYPE_DATABASE = 'database'
COMPONENT_TYPE_PLACEHOLDER = 'placeholder'
CREATE_FLAG = 'create'
MODIFY_FLAG = 'modify'
PORTS_KEY = 'ports'
DESCRIPTION_KEY = 'description'
COMPONENTS_KEY = 'components'
PIPELINES_KEY = 'pipelines'
CONTROLLER_FIELD = "controller"
SERVER_URL_FIELD = "server_url"
UTF8_ENCODING = "utf-8"
ONE_GB_IN_BYTES = 1073741824
HUNDRED_MB_IN_BYTES = 104857600
COMPONENT_FLAVOR_PYSPARK = "pyspark"
DASHBOARD_PORT_CONFIG = 'dashboard_port'
ACTIVATION_STATUS_KEY = "activationStatus"
VERSION_ID_RESPONSE_KEY = 'version_id'
STRATEGY_USED_KEY = 'strategy_used'
INDEX_KEY = 'index'
LOCAL_RUN_FLAG = "local_run"
DOCKER_IMAGE_PLACEHOLDER = "XPRESSO_DOCKER_REGISTRY"

COMPONENT_NAME_KEY = "name"
COMPONENT_TYPE_KEY = "type"
LIBRARY_COMPONENT_KEY = "library"

CLIENT_PATH_FIELD = "client_path"
PY_EXTENSION = ".py"
ZIP_EXTENSION = ".zip"

IS_GPU_CLUSTER_KEY = "is_gpu"
IS_XPRESSO_RUN_NAME_REQUIRED_KEY = "is_xpresso_run_name_required"

# Deployment JSON Constants
PERSISTENCE_KEY = 'persistence'
ENVIRONMENT_PARAMETERS_KEY = 'environment_parameters'
ARGS_KEY = 'args'
COMMAND_KEY = 'command'
ENVIRONMENT_PARAMETERS_NAME_KEY = 'name'
ENVIRONMENT_PARAMETERS_VALUE_KEY = 'value'
TARGET_ENVIRONMENT_KEY = 'target_environment'

# Project Management
REPO_NAME_PREFIX = "xpr"
REPO_NAME_SUFFIX = "sc"

# This is the environment variable that is used to identify xpresso directory
ENV_XPRESSO_PACKAGE_PATH = "XPRESSO_PACKAGE_PATH"
DEFAULT_XPRESSO_PACKAGE_PATH = root_package_location
ENV_XPRESSO_CONFIG_PATH = "XPRESSO_CONFIG_PATH"
DEFAULT_XPRESSO_CONFIG_PATH = "config/common.json"

# Pipeline constants
FLAVOR_KEY_IN_PIPELINES = "flavor"
PIPELINE_FLAVOR_KUBEFLOW = 'kubeflow'
PIPELINE_FLAVOR_BIGDATA = 'pyspark'
PIPELINE_FLAVOR_ABSTRACT = 'abstract'
PIPELINE_FLAVOR_DEFAULT = PIPELINE_FLAVOR_KUBEFLOW
PROJECT_NAME_KEY = 'name'
PROJECT_COMPONENTS_KEY = 'components'
PROJECT_GIT_URL = "giturl"
PROJECT_CONFIG_FLAVOR_DOCKER_MAP = "flavor_base_docker_image"
variable_indicator = "##"
PROJECTS_SECTION = 'projects'
DECLARATIVE_PIPELINE_FOLDER = 'declarative_pipeline_folder'
KUBEFLOW_TEMPLATE = 'kubeflow_template'
XPRESSO_MOUNT_PATH = 'XPRESSO_MOUNT_PATH'
open_parenthesis = '('
single_quote = "'"
double_quote = '"'
escape_quote = '\\"'
test_deploy_id = 0
pipeline_conflict_error_code = 500
istio_gateway_service = 'istio-ingressgateway'
kubeflow_namespace = 'kubeflow'
istio_namespace = 'istio-system'
http2_port = 'http2'
kubeflow_installation_directory_name = 'kubeflow'
kubeflow_download_url = 'https://github.com/kubeflow/kubeflow/releases' \
                        '/download/v0.6.1/kfctl_v0.6.1_linux.tar.gz'
kubeflow_config_url = 'https://raw.githubusercontent.com/kubeflow/kubeflow/v0' \
                      '.6.1/bootstrap/config/kfctl_k8s_istio.yaml'
kubeflow_pvc_yaml_path = 'config/kubeflow-persistent-volumes.yaml'
RUN_PARAMETERS_KEY = 'run_parameters'
KUBEFLOW_CONFIG_KEY = "kubeflow"
MOUNT_LOCATION_PREFIX_CONFIG_KEY = "pv_mount_location_prefix"
MOUNT_LOCATION_PREFIX_USER = "pv_mount_location_prefix_user"
MOUNT_LOCATION_PREFIX_PROJECT = "pv_mount_location_prefix_project"

# Declarative JSON constants
DECLARATIVE_JSON_TEMPLATE = {
    "name": "",
    "description": "",
    "components": [],
    "after_dependencies": {}
}
DECLARATIVE_JSON_COMPONENT_TEMPLATE = {
    "name": "",
    "implementation": {
        "container": {
            "command": [],
            "args": []
        }
    }
}
PIPELINE_INPUT_STUB = \
    '{"name": "XPRESSO_INPUT_NAME", "type": "String", "description": ""}'
INPUT_VALUE_KEY = 'inputValue'
AFTER_DEPENDENCIES_KEY = 'after_dependencies'
INPUTS_KEY = 'inputs'
CONTAINER_KEY = 'container'
IMPLEMENTATION_KEY = 'implementation'
IMAGE_KEY = 'image'
DECLARATIVE_JSON_KEY = 'declarative_json'
E = 'run_id'

# Kwargs used during deployment
pv_size_kwarg = "pv_size"
master_node_kwarg = "master_node"
pipeline_name_kwarg = "pipeline_name"
pipeline_versions_dict_kwarg = "pipeline_versions"
target_env_kwarg = "target_env"
mesh_name_kwarg = "mesh_name"
secure_port_kwarg = "secure_port"
parent_app_name_kwarg = "parent_app_name"
versions_list_kwarg = "versions_list"
is_gpu_kwarg = 'is_gpu'
service_name_kwarg = 'service_name'
namespace_kwarg = 'namespace'
token_kwarg = 'token'
new_components_kwarg = 'new_components'

GPU_LIMIT = 1

# Local test repo for testing skeleton string replace
LOCAL_TEST_REPO = "skeleton-build"

# DB Setup
replset_initiate_cmd = "replSetInitiate"

# Bundle Input Json
bundle_local = "local"
bundle_cluster = "cluster"
xpresso_cluster_bundle = "XpressoClusterBundle"
xpresso_modular_cluster_bundle = 'XpressoModularClusterBundle'

# Kubernetes Constants
pv_size_cap = 10
kubernetes_GB_suffix = 'Gi'
type_pvc = "volume-claim"
type_pv = "volume"
pipeline_pv_prefix = "pipeline"
DEPLOYMENT_FILES_FOLDER = 'deployment_files_folder'
USE_HOSTNAME = "use_hostname"
pipelines_storage_class = "pipelines"
deployments_storage_class = "deployments"
database_storage_class = "databases"
k8_conflict_error_code = 409
DEPLOYMENT_FLAVOR_KUBERNETES = "kubernetes"
k8_secure_port = '6443'
SVC_TYPE_NODEPORT = "NodePort"
K8_RESOURCE_TYPE_SERVICE = 'service'
K8_RESOURCE_TYPE_NAMESPACE = 'namespace'
K8_RESOURCE_TYPE_VOLUME = 'volume'
K8_RESOURCE_TYPE_DEPLOYMENT = 'deployment'
K8_RESOURCE_TYPE_JOB = 'job'
K8_RESOURCE_TYPE_CRONJOB = 'cronjob'
K8_RESOURCE_TYPE_SECRET = 'secret'
K8_RESOURCE_TYPE_ROLEBINDING = 'rolebinding'
K8_RESOURCE_TYPE_NODE = 'node'
K8_RESOURCE_TYPE_CUSTOM = 'custom'
K8_NODEPORT_KEY = 'nodePort'

# Spark Constants
DEPLOYMENT_FLAVOR_SPARK = "spark"

# xprctl
help_option_long = '--help'
help_option_short = '-h'
version_option_full = '--version'
OUTPUT_COLOR_GREEN = "green"
OUTPUT_COLOR_RED = "red"

# mongodb constants
port = '27017'
path = '/home/abzooba/Desktop/backup'
pipeline_versions_field = 'pipeline_versions'
pipeline_id_field = 'pipeline_id'
pipeline_ip_field = 'pipeline_ip'
pipeline_name_field = 'name'
component_parameters_field = "components_parameter"
PIPELINE_COMPONENTS_FIELD = 'components'
pipeline_version_id_field = 'version_id'
PROJECT_NAME_IN_PIPELINES = "project_name"
PROJECT_NAME_IN_PROJECTS = "name"
PROJECT_TOKEN_IN_PROJECTS = "project_token"
PULL_OPERATION_MONGO = "pull"
JUPYTERHUB_KEY = "jupyterhub"

# Mongo config DB
CONFIG_MONGO_SECTION = 'mongodb'
CONFIG_MONGO_URL = 'mongo_url'
CONFIG_MONGO_DB = 'database'
CONFIG_MONGO_UID = 'mongo_uid'
CONFIG_MONGO_PWD = 'mongo_pwd'
CONFIG_MONGO_W = 'w'
CONFIG_MONGO_EXPERIMENT = "experiment_collection"
CONFIG_MONGO_RUN = "run_collection"
CONFIG_MONGO_BACKUP_LOCATION = 'backup_directory'

MONGO_DOC_FILTER = "doc_filter"
MONGO_PROJECTION_FILTER = "projection_filter"
MONGO_OUTPUT_LIMIT = "limit"
MONGO_UPDATE_FLAG = "flag"
MONGO_UPDATE_INFO = "obj"
# Backup interval in Days
CONFIG_MONGO_BACKUP_INTERVAL = 'backup_interval'

# Mongo Collections
PIPELINE_COLLECTIONS = "pipelines"
PROJECT_COLLECTIONS = "projects"
USER_COLLECTIONS = "users"
EXPERIMENT_COLLECTIONS = "experiments"
DEPLOYMENTS_COLLECTION = "deployments"
CLUSTERS_COLLECTION = "clusters"
SCHEDULES_COLLECTION = "schedules"
RUNS_COLLECTION = "runs"
BRANCHES_COLLECTION = "branches"
MASTER_NODES_KEY_CLUSTERS_COLLECTION = "master_nodes"
ADDRESS_KEY_CLUSTERS_COLLECTION = "address"
CLUSTER_TYPE_KEY_CLUSTERS_COLLECTION = "cluster_type"
FOR_JUPYTER_KEY = "for_jupyter"
EVENTS_COLLECTION = "events"
BASE_IMAGES_COLLECTION = "base_images"

# Confluence manual links
user_manual_link = "https://abzooba.atlassian.net/wiki/spaces/XS/overview"
project_manual_link = "https://abzooba.atlassian.net/wiki/spaces/XS/overview"
start_experiment_manual_link = "https://abzooba.atlassian.net/wiki/spaces/XP/pages/993723805/Start+experiment"
follow_project_manual_message = "Please follow the project manual " \
                                f"{project_manual_link} and try again."
follow_user_manual_message = f"Please follow the user manual " \
                             f" {user_manual_link} and try again."
follow_start_exp_manual_message = \
    f"Please follow the start experiment manual" \
    f" {start_experiment_manual_link} and try again."

# data module constants
DEFAULT_OUTLIER_THRESHOLD_CATEGORICAL = 2
DEFAULT_OUTLIER_MARGIN_NUMERIC = 10
DEFAULT_ATTRIBUTE_VALIDITY_THRESHOLD = 95
DEFAULT_PROBABILITY_BINS = 15
DEFAULT_MODE_COUNT = 50
PERCENT_SIGN = "%"
SPACE_STRING = " "
LINE_SPACE = 10
DAY = 'D'
DEFAULT_SUBPLOT_COLUMNS = 2
UNIGRAM = 'grams_1'
BIGRAM = 'grams_2'
TRIGRAM = 'grams_3'
STOPWORD = 'stopword'
TEST_DATA_PATH = "./config/test/data/test.csv"
TEST_INVALID_DATA_PATH = "./config/test/data/test_invalid.csv"
DATASET_DIFFERENCE_PATH = "./DatasetDifference/"
DATA_DIFFERENCE_FILENAME = "data_difference.xlsx"
METADATA_DIFFERENCE_FILENAME = "metadata_difference.xlsx"

# Unstructured Module constants
FILE_NAME_COL = "File Name"
FILE_PATH_COL = "Path"
FILE_SIZE_COl = "Size (in MB)"

# authentication constants
mongodb_auth_type = "mongodb"
ldap_auth_type = "ldap"

#  Data versioning constants
# user input variables
DV_INPUT_BRANCH_NAME = "branch_name"
DV_INPUT_COMMIT_ID = "commit_id"
DV_INPUT_DATASET_NAME = "dataset_name"
DV_INPUT_DESCRIPTION = "description"
DV_INPUT_REPO_NAME = "repo_name"
DV_INPUT_PATH = "path"
DV_INPUT_DATASET = "dataset"
DV_INPUT_UID = "uid"
DV_INPUT_PASSWORD = "pwd"
DV_INPUT_ENV = "env"
DV_PROJECT_TOKEN = "project_token"
DV_USER_TOKEN_FLAG = "user_token_flag"
DV_PROJECT_TOKEN_FLAG = "project_token_flag"
DV_SERVER_REQUEST_FLAG = "server_request_flag"
DV_SERVER_AUTH_TOKEN = "server_auth_token"
DV_RUN_PARAMETERS_BRANCH = "run_parameters"
DV_RUN_PARAMETERS_DATASET = "input"
DV_RUN_PARAMETERS_PATH = f"/dataset/{DV_RUN_PARAMETERS_DATASET}/"
DV_RUN_PARAMETERS_FILENAME = "parameters.json"
DV_RUN_PARAMETERS_FOLDER_PREFIX = "run_parameters"
DV_BRANCH_TYPE_DATA = "data"

# Data Cleaning constants

# input dict key parameters
deduplicate_rows = "deduplicate"
considered_columns = "columns"
keep_row = "keep"
date = "date"
date_format = "format"
clean_dates = "clean_dates"

# Data connection constants
KEY_DATA_SOURCE = "data_source"
KEY_TYPE = "type"
KEY_PATH = "path"
VALUE_LOCAL = "local"

# input json key parameters
DSN = "DSN"
table = "table"
columns = "columns"
input_path = "path"
delimiter = "delimiter"
dataset_type = "dataset_type"
distributed = "distributed"

# BigQuery constants
cred_path = "cred_path"
project_id = 'project_id'
dataset = 'dataset'

# common.json parameters
connectors = "connectors"
presto = "presto"
presto_hostname = "presto_host"
presto_port = "presto_port"
presto_user = "presto_user"
root_hosts_path = "hosts_path"
catalog = "catalog"
schema = "schema"
alluxio = "alluxio"
alluxio_ip = "alluxio_ip"
alluxio_port = "alluxio_port"
hdfs = "hdfs"
hdfs_ip = "hdfs_ip"
hdfs_port = "hdfs_port"

# conversion factor for converting bytes to Megabytes
CONV_FACTOR = 1000 * 1000

# directory which will be created to download and save the raw files
DOWNLOAD_DIR = 'Downloaded_Files'

# path to data connector module
dataconnector_path = 'xpresso/ai/core/data/connector'

# the supported filetypes for data connector class
COMMA = ","
PIPE = "|"
DOT = '.'
csv_extension = ['.csv', '.data', '.txt']
excel_extension = ['.xlsx', ".xls"]
json_extension = ".json"
hdfs_URI = "hdfs://{}:{}{}"

# data connector constants
HTTPS = "https"

USER_DB_CONSTANTS = {
    "UID": "uid",
    "PASSWORD": "pwd",
    "TOKEN": "token"
}

config_paths = {
    "default": "config/common.json",
    "dev": "config/common_dev.json",
    "prod": "config/common_prod.json",
    "qa": "config/common_stage.json",
    "sandbox": "config/common_sandbox.json"
}
DEFAULT_CONFIG_PATH_SETUP_LOG = "config/setup_docker.json"

# Visualization Command
PLOTLY_ORCA_PORT = 48000

# Exploration default paths and filenames
MAX_DATASET_SIZE_GB = 1  # Size in GB
MAX_DATE_LENGTH = 26  # Date "Wednesday 26 February 2019"
MIN_DATE_LENGTH = 5  # Date "11/13"
N_GRAM_VALUE = 3
EXPLORER_OUTPUT_PATH = "./Exploration/"
EXPLORE_UNIVARIATE_FILENAME = "explore_univariate.xlsx"
EXPLORE_MULTIVARIATE_FILENAME = "explore_multivariate.xlsx"
EXPLORER_UNSTRUCTURED_FILENAME = "explore_unstructured.xlsx"
# max sheet size supported by ExcelWriter
MAX_SHEET_SIZE = {"rows": 1048576, "columns": 16384}
REPORT_OUTPUT_PATH = "./Report/"
REPORT_UNIVARIATE_FILENAME = "report_univariate.pdf"
REPORT_MULTIVARIATE_FILENAME = "report_multivariate.pdf"

DEFAULT_GARBAGE_THRESHOLD = 5  # percent
PIE_CHART_OTHERS_THRESHOLD = 2  # percent for better visibility of categories
BAR_CHART_TAIL_THRESHOLD = 5  # percent for better visibility of significant
# bars

# Distributed EDA
TEMP_DIRECTORY = "./tmp/"
PARQUET_EXT = ".parquet"

# data serializtion constants
NGRAMS = ["unigram", "bigram", "trigram"]
CORRELATIONS = ["chi_square", "spearman", "pearson"]

# Controller Stub
STATUS = "status"
METRIC = "metric"
MESSAGE = "message"
OUTPUT_DIR = "/output/"
BRANCH_EXTENTION = "_model"
TIMEFORMAT = "%Y-%m-%d %H:%M:%S"
PKL_EXT = ".pkl"
DESCRIPTION = "Pushing output folder to pachyderm for model versioning"

# Access Roles
DEVELOPER = "dev"
PROJECT_MANAGER = "pm"
ADMIN = "admin"
SUPER_USER = "su"
PROJECT_OWNER = "owner"
NO_CHECK = "no_check"

# Project Constants
PIPELINES_KEY_IN_PROJECTS = "pipelines"
PIPELINE_NAME_IN_PROJECTS = "name"
VERSION_ID_KEY_IN_PROJECTS = "deploy_version_id"
FLAVOR_KEY_IN_PROJECTS = "flavor"
OWNER_ID_KEY_IN_PROJECTS = "uid"
DEVELOPERS_KEY_IN_PROJECTS = "developers"
FLAVOR_KEY_PROJECT_COMPONENTS = "flavor"
PROJECT_DEVELOPERS = "developers"
KEY_DOCKER_IMAGE = "dockerImage"
KEY_VERSIONS = "versions"
KEY_DOCKER_PREFIX = "dockerPrefix"
PERSISTENT_VOLUME_SIZE_KEY = "persistent_volume_size"

PROJECT_DISPLAY_KEY_RENAME_DICT = {
    "giturl": "Code Versioning Repo"
}

# Pipeline collection Constants
PROJECT_NAME_PIPELINE_COLLECTIONS = "project_name"
PIPELINES_KEY_IN_PIPELINES = "pipelines"
PIPELINE_NAME_KEY_IN_PIPELINES = "name"
PIPELINE_VERSIONS_KEY_PIPELINES = "pipeline_versions"
VERSION_ID_KEY_PIPELINES = "version_id"
PIPELINE_ID_KEY_PIPELINES = "pipeline_id"
COMPONENTS_KEY_IN_PIPELINES = "components"
RUN_BY = "run_by"
START_TIME = "start_time"
LAST_UPDATE_TIME = "last_update_time"
PIPELINE_STATUS = "status"

# XprResponse constants
OUTCOME_SUCCESS = "success"
OUTCOME_FAILURE = "failure"

# Data Versioning constants
VERSIONING_TOOL = "tool"
VERSIONING_CONFIG = "data_versioning"
VERSIONING_SERVER = "server"
VERSION_TOOL_HOST = "cluster_ip"
VERSION_TOOL_PORT = "port"
PACHYDERM_IN_CONFIG = "pachyderm"
OUTPUT_TYPE_FILES = "files"

# Experiment methods
CREATE_EXP = "create_experiment"
GET_EXP = "get_experiment_details"
START_EXP = "start_experiment"
CONTROL_EXP = "control_experiment"
PAUSE_EXP = "pause_experiment"
RESTART_EXP = "restart_experiment"
TERMINATE_EXP = "terminate_experiment"

# Abstract Pipeline Component
ENABLE_LOCAL_EXECUTION = "enable_local_execution"
PARAMS_FILENAME_ENV = "PARAMETERS_FILENAME"
PARAMS_COMMIT_ID_ENV = "PARAMETERS_COMMIT_ID"

BLANK = ""
TMP_DIR = "/tmp"

# Project Object
PROJECT_CFG_INTERNAL_FIELD = "internal_output_fields"

# Code repo default branch
DEFAULT_BRANCH = "master"

# Project Creation Steps
PROJECT_CREATED = "project_created"
REPO_CREATED = "repo_created"
NFS_FOLDER_CREATED = "nfs_directory_created"
HDFS_FOLDER_CREATED = "hdfs_directory_created"
DB_UPDATED = "db_updated"
ENV_ALLOCATED = "env_allocated_to_project"
BUILD_PIPELINE_CREATED = "created_build_pipeline"
PIPELINE_SETUP_COMPLETED = "deploy_pipelines-setup_complete"
PROJECT_UPDATED = "project_updated"
LOCAL_PROJECT_CREATED = "project_created_locally"
COMPONENT_DELETED = "component deleted successfully"

# User Input Constants for Project ops
INPUT_PROJECT_NAME = "name"
INPUT_PROJECT_DESCRIPTION = "projectDescription"
INPUT_PROJECT_COMPONENTS = "components"
INPUT_PROJECT_PIPELINES = "pipelines"
INPUT_NORMAL_ENVIRONMENT = "environments"
INPUT_BIGDATA_ENVIRONMENT = "bigdata_environments"
INPUT_PROJECT_COMPONENT_NAME = "name"
COMPONENT_BASE_IMAGE = "base_image"

# BigData
SPARK_DEPLOY_BASE_DIR = "/opt/xpr-spark-deployments"
KEY_SPARK_CLUSTER = "spark_cluster"
KEY_SPARK_CLUSTER_MANAGER = "cluster_manager"
KEY_SERVICE_ACCOUNT = "service_account"
KEY_SPARK_K8S_SECRET = "secret"
KEY_SPARK_HOME = "spark_home"
SPARK_KEY_CLUSTER_ADMIN = "cluster_admin"
SPARK_KEY_MASTER = "master"
SPARK_KEY_JOB_UI_PORT = "job_ui_port"
SPARK_KEY_HOST = "host"
KEY_RUN_PARAMETERS = "run_parameters"
KEY_BRANCH = "branch"
KEY_COMMIT_ID = "commit_id"
SPARK_KEY_DEFAULT_RESOURCE_LIMITS = "default_resource_limits"
KEY_DRIVER_MEMORY = "driver_memory"
KEY_EXECUTOR_MEMORY = "executor_memory"
KEY_EXECUTOR_CORES = "executor_cores"
KEY_NUM_EXECUTOR = "num_executors"
KEY_JOB_NAME = "job_name"
KEY_RUN_NAME = "xpresso_run_name"
SPARK_K8S_CM = "k8s"
SPARK_YARN_CM = "yarn"
SPARK_KEY_PORT = "port"
SPARK_K8S_SVC_ACCOUNT_NAME = "spark"
SPARK_K8S_ROLEBINDING_NAME = "spark-role"

# Service Mesh constants
SERVICE_MESH_KEY = 'service_mesh'
SERVICE_MESH_NAME_KEY = 'name'
SERVICE_MESH_COMPONENTS_KEY = 'components'
SERVICE_MESH_FLAVOR_ISTIO = 'istio'
SERVICE_MESH_STRATEGY_KEY = 'strategy'
SERVICE_MESH_VERSION_NAME_KEY = 'version_name'
ISTIO_API_VERSION = 'v1alpha3'
ISTIO_API_GROUP = 'networking.istio.io'
ISTIO_DESTINATION_RULE_PLURAL = 'destinationrules'
ISTIO_VIRTUAL_SERVICE_PLURAL = 'virtualservices'
ISTIO_WEIGHTS_STRATEGY_KEY = 'weights'
ISTIO_RULE_STRATEGY_KEY = 'rule'
ISTIO_HEADER_RULE_KEY = 'header'
HEADER_MATCH_TYPE_KEY = 'match_type'
HEADER_MATCH_STRING_KEY = 'match_string'
HEADER_VALUE_KEY = 'header_value'
ISTIO_DESTINATION_KEY = 'destination'
ISTIO_DEFAULT_DESTINATION_KEY = 'default_destination'
MATCH_TYPE_EXACT = 'exact'
MATCH_TYPE_REGEX = 'regex'
MATCH_TYPE_PREFIX = 'prefix'
istio_subsets_stub = '{"name": "ISTIO_VERSION_NAME", ' \
                     '"labels": {"version": "ISTIO_VERSION_NAME"}}'
istio_route_stub = '{"destination": {"host": "ISTIO_PARENT_APP_NAME", ' \
                   '"subset": "ISTIO_VERSION_NAME"},' \
                   '"weight": ISTIO_VERSION_WEIGHT}'
istio_match_header_stub = \
    '{"match": [{"headers": {"ISTIO_XPRESSO_HEADER_VALUE": ' \
    '{"ISTIO_XPRESSO_MATCH_TYPE": "ISTIO_XPRESSO_MATCH_STRING"}}}], ' \
    '"route": [{"destination": {"host": "ISTIO_XPRESSO_PARENT_APP",' \
    '"subset": "ISTIO_XPRESSO_DESTINATION"}}]}'
istio_header_default_route_stub = \
    '{"route": [{"destination": {"host": "ISTIO_XPRESSO_PARENT_APP", ' \
    '"subset": "ISTIO_XPRESSO_DEFAULT_DESTINATION"}}]}'
GRAFANA_DASHBOARD_SUFFIX_CONFIG = "dashboard_info_suffix"
GRAFANA_SVC_NAME = "grafana"
APP_LABEL_KEY = 'app'

# User DB Constants
PRIMARY_ROLE = "primaryRole"
UID = "uid"
PWD = "pwd"
TOKEN = "token"

# Inference Service constants
INFERENCE_SERVICE_KEY = 'inference_service'
RUNS_KEY = 'runs'
auto_mesh_creation_template_path = 'config/auto_mesh_creation_template'
MESH_NAME_KEY = 'mesh_name'
modify_project_json_stub = \
    '{"name" : "XPRESSO_PROJECT_NAME","service_mesh": [' \
    '{"name": "XPRESSO_INFERENCE_SERVICE_NAME",' \
    '"description": "Auto generated by xpresso platform",' \
    '"components": XPRESSO_INFERENCE_SERVICE_COMPONENTS}]}'
service_mesh_components_stub = \
    '{"version_name": "vXPRESSO_VERSION_NUMBER", "replicas": 1, ' \
    '"custom_docker_image": "XPRESSO_CUSTOM_DOCKER_IMAGE", ' \
    '"build_version": "XPRESSO_BUILD_VERSION", "environment_parameters": [' \
    '{"name": "REPO_NAME", "value": "XPRESSO_PROJECT_NAME"}, ' \
    '{"name": "BRANCH_NAME", "value": "XPRESSO_MV_BRANCH_NAME"}, ' \
    '{"name": "COMMIT_ID", "value": "XPRESSO_COMMIT_ID"}, ' \
    '{"name": "DATA_VERSIONING_PATH","value": "XPRESSO_DV_PATH"}, ' \
    '{"name": "RUN_NAME","value": "XPRESSO_RUN_NAME"}],' \
    '"ports": XPRESSO_PORTS_ARRAY}'
INFERENCE_DEFAULT_MOUNT_PATH = "/inf_data"

# Build API constants
BUILD_PROJECT_NAME_KEY = "project_name"
BUILD_COMPONENT_NAME_KEY = "component_name"
BUILD_PIPELINE_NAME_KEY = "pipeline_name"
BUILD_STAGES_KEY = "stages"
BUILD_COMPONENT_KEY = "component"
BUILD_PIPELINE_KEY = "pipeline"
BUILD_IS_LOG_KEY = "is_log"
BUILD_ID_KEY = "build_id"
BUILD_MESSAGE_KEY = "build_message"
BUILD_TIME_TAKEN_KEY = "time_taken"
BUILD_BRANCH_NAME_KEY = "branch_name"
BUILD_BRANCH_COMMIT_KEY = "branch_commit"
BUILD_CREATED_BY_KEY = "created_by"
BUILD_CREATED_ON_KEY = "created_on"
BUILD_COMPLETED_ON_KEY = "completed_on"
build_stages = "build_stages"
BUILD_STAGE_FLOW_KEY = "stage_flow"
NODE_ID_KEY = "id"
STAGE_FLOW_LOGS_KEY = "logs"
STAGE_FLOW_NODES_KEY = "stageFlowNodes"
ERROR_MESSAGE_KEY = "error_message"
ERROR_KEY = "error"
PARAMETER_DESCRIPTION_KEY = "parameterDescription"
LINKS_KEY = "_links"
LOG_KEY = "log"
HREF_KEY = "href"
TEXT_KEY = "text"
VERSION_ID_KEY = "version_id"
VERSION_DESCRIPTION_KEY = "version_description"
VERSION_CREATED_BY_KEY = "submitted_by"
VERSION_CREATED_ON_KEY = "submitted_on"
VERSION_BRANCH_NAME_KEY = "branch"
VERSION_BRANCH_COMMIT_KEY = "commit_id"
TOTAL_BUILDS_KEY = "total_builds"
BUILD_HISTORY_KEY = "build_history"
LATEST_BUILD_DETAILS_KEY = "latest_build_details"
DB_BUILD_INFO = "build_info_from_db"
IN_PROGRESS_STATUS = "IN-PROGRESS"
DEFAULT_TIME_TAKEN = "0:00:00"
DURATION_IN_MILLISECONDS = "durationMillis"
BUILD_DEFAULT_UID = "xpradmin"
BUILD_DEFAULT_DESCRIPTION = "Sample description"

# Pipeline API constants
EXPERIMENT_PROJECT_NAME_KEY = "project_name"
EXPERIMENTS_KEY = "experiments"
PIPELINE_EXP_HISTORY = "experiment_history"
PIPELINE_RUN_ID_KEY = "pipeline_run_id"
RUN_STARTED_BY = "started_by"
RUN_STATUS = "run_status"
RUN_OUTPUT = "output"
RUN_NAME = "run_name"
XPRESSO_RUN_NAME = "xpresso_run_name"
RUN_PIPELINE_VERSION = "pipeline_version"
END_TIME = "end_time"
TIME_TAKEN = "time_taken"
EXPERIMENT_NAME = "experiment_name"
EXPERIMENT_MASTER_NODE = "master_node"
EXPERIMENT_ID = "experiment_id"
RUN_ID_KEY = "run_id"
RUN_LINK = "run_link"
TOTAL_RUNS = "total_runs"
RUN_HISTORY = "run_history"
RUN_STATUS_OVERVIEW_KEY = "runs_status_overview"
LATEST_RUN_DETAILS = "latest_run_details"
EXPERIMENT_RUN_DETAILS_KEY = "run_details"
COMPONENTS_STATUS_OVERVIEW_KEY = "components_status_overview"
COMPONENTS_STATUS_KEY = "components_status"
COMPONENT_NAME_IN_RUN_INFO_KEY = "component_name"
RUN_METRICS_STATUS = "run_metrics_status"
NODES_KEY = 'nodes'
DISPLAYNAME_KEY = 'displayName'
POD_KEY = 'Pod'
POD_NAME_KEY = 'pod_name'
COMPONENTNAME_KEY = 'component_name'
DOCKER_IMAGE_KEY = 'docker_image'
VOLUME_MOUNTS_KEY = 'volume_mounts'
ENVIRONMENT_VARIABLES_KEY = 'environment_variables'
ARGUMENTS_KEY = "arguments"
KUBEFLOW_RUN_RESOURCE = "kubernetes"
KUBEFLOW_RUN_RESOURCE_CONTAINER_NAME = 'main'
TERMINATED_STATUS = "terminated"
RUNNING_STATUS = "running"
RESOURCE_ID = "id"
RESOURCE_LINK = "link"
DATE_FORMAT = '%Y-%m-%d %H:%M:%S%z'
PIPELINE_NAME_KEY = "pipeline_name"
SPARK_RUN_RESOURCE_CONTAINER_NAME = 'spark-kubernetes-driver'
SPARK_ON_K8S_RUN_RESOURCE = "spark_on_k8s"
LOGS_KEY = "logs"

# SSL Config
CONFIG_SSL = "ssl"
CONFIG_SSL_CERT_PATH = "cert_path"
CONFIG_SSL_PKEY_PATH = "pkey_path"
CONFIG_SSL_VERIFY = "verify"

# User related constants
USER_INPUT_UID = "uid"
UID_KEY_IN_DB = "uid"
USER_INPUT_PWD = "pwd"
PWD_KEY_IN_DB = "pwd"
TOKEN_KEY_IN_DB = "token"
SOFT_EXPIRY_KEY_IN_DB = "loginExpiry"
HARD_EXPIRY_KEY_IN_DB = "tokenExpiry"

# Miscellaneous
DOCKER_AUTH_STUB = '{"auths":{"XPRESSO_DOCKER_HOST/":{"username":' \
                   '"XPRESSO_DOCKER_UID","password":"XPRESSO_DOCKER_PWD",' \
                   '"auth":"XPRESSO_DOCKER_AUTH"}}}'

# Code Structure
CONFIG_STRUCTURE_KEY = "structure"
CONFIG_LIB_STRUCTURE_KEY = "library_structure_list"
CONFIG_COMPONENT_SCRIPTS_KEY = "component_scripts"

# Token
TOKEN_EXPIRED = "token_expired"
LOGIN_EXPIRED = "login_expired"
NOT_EXPIRED = "not_expired"

# Dashboard
CONFIG_DASHBOARD = "dashboard"
CONFIG_DASHBOARD_BITBUCKET = "bitbucket"
CONFIG_DASHBOARD_KUBERNETES = "kubernetes"
CONFIG_DASHBOARD_JENKINS = "jenkins"
CONFIG_DASHBOARD_KIBANA = "kibana"
CONFIG_DASHBOARD_NFS = "nfs"
CONFIG_DASHBOARD_KUBEFLOW = "kubeflow"

CONFIG_BUNDLE_SETUP = "bundles_setup"
CONFIG_BUNDLE_SETUP_NFS = "nfs"
CONFIG_BUNDLE_SETUP_NFS_LOCATION = "nfs_location"

# Monitoring
XPRESSO_MONITORING_NAMESPACE = 'xpresso'
KUBE_SYSTEM_NAMESPACE = "kube-system"
SENDER_EMAIL = "alerts-xpresso@abzooba.com"
SENDER_PASSWORD = "+iytzXD6G+TpPWF3TxDXaTQ9voZWkG6tgjxilMWRoxA="
XPRESSO_MONITORING_CONFIG_FILE = '/opt/status_config.json'

# User Context Constants
USER_CONTEXT = "user_context"
CONTEXT_USER_INFO_KEY = "user_info"
CONTEXT_PROJECT_LIST_KEY = "project_list"

DEFAULT_PASSWORD_VALUE = "GVVzuDa9knSTXMsdD7xhMxfXSaPurDApJffcdxPvLsqe2WsOvAOkTmBFIuXfYe"

ACTION_TYPE_KEY = "action_type"
ACTION_TYPE_DELETE = "delete"

OUTPUT_LIMIT_KEY = "limit"

# Experiment Manager input constants
VERBOSE = "verbose"
EXPERIMENTS_INPUT_KEY = "experiments"
PARAMETERS_FILENAME_KEY = "parameters_filename"
PARAMETERS_COMMIT_ID_KEY = "parameters_commit_id"
PARAMETERS_KEY = "parameters"
EMPTY_PARAMETERS_FIELD_VALUE = "None"

# jupyter auto_project
notebooks_mount_path = "notebooks_path"
json_template_folder = "json_template_folder"
JUPYTER_EXPERIMENTS_SECTION = "jupyter_auto_project"
NOTEBOOK_PROJECTS_BASE_PATH_KEY = "notebook_projects_base_path"
FOLDERS = ["code", "scripts"]

# Deploy Response Keys
RESPONSE_MESSAGE_KEY = 'message'
RESPONSE_SERVICE_IPS_KEY = 'service_ips'
RESPONSE_DASHBOARD_IP_KEY = 'dashboard_ip'
RESPONSE_PIPELINES_INFO_KEY = 'pipelines_info'
RESPONSE_DEPLOY_HOST_KEY = 'deploy_host'

# Deployments Collection Constants
PROJECT_NAME_DEPLOYMENTS_COLLECTION = 'project_name'
COMPONENTS_KEY_DEPLOYMENTS_COLLECTION = 'components'
MESH_KEY_DEPLOYMENTS_COLLECTION = 'service_mesh'
DEPLOYED_VERSIONS_KEY = 'deployed_versions'
COMPONENT_NAME_KEY_DEPLOYMENTS_COLLECTION = 'component_name'
DEPLOY_INFO_KEY_DEPLOYMENTS_COLLECTION = 'deploy_info'
DEPLOYED_ENVIRONMENTS = 'deployed_environments'
DEPLOYED_TIME = "deployed_at"
DEPLOYED_BY = "deployed_by"
VERSION_ID_KEY_DEPLOYMENTS_COLLECTION = "deploy_version_id"

# Build project config constants
BUILD_CONFIG = "build_management"
BUILD_TOOL = 'build_tool'
BUILD_HOST = 'master_host'

# Metrics config constants
METRICS_CONFIG_KEY = "metrics"
CACHE_LENGTH_LIMIT = "cache_length_limit"
CACHE_SIZE_LIMIT = "cache_size_limit"
CACHE_INIT_TIME_LIMIT = "cache_init_time_limit"

# Code management config
CONFIG_CODE_MANAGEMENT = "code_management"
CONFIG_SKELETON_PATH = "skeleton_path"
CONFIG_SKELETON_BRANCH = "skeleton_branch"

# File explorer constants
NFS_EXPLORER_FLAG = "nfs_explorer"
HDFS_EXPLORER_FLAG = "hdfs_explorer"
EXPLORER_FLAG = "api_flag"

# Default Config Workspace
DEFAULT_WORKSPACE = "default"

# request despatcher
REQUESTS_COLLECTION = 'requests'

# Schedules Collection Constants
PROJECT_NAME_SCHEDULES_COLLECTION = "project_name"
SCHEDULES_KEY_SCHEDULES_COLLECTION = "schedules"
IS_RECURRING_KEY = "is_recurring"
RECURRING_FREQUENCY_KEY = "recurring_frequency"
SCHEDULE_DATE_KEY = "schedule_date"
RECURRING_DATE_KEY = "recurring_date"
RECURRING_DAY_KEY = "recurring_day"
RECURRING_MONTH_KEY = "recurring_month"
SCHEDULE_TIME_KEY = "schedule_time"
TIME_SEPARATOR = ":"
DATE_SEPARATOR = "/"
CRON_ASTERISK = "*"
CRONTAB_EXPRESSION_KEY = "crontab_expression"
SCHEDULE_STATUS_KEY = "status"
SCHEDULE_ACTION_KEY = "action"
SCHEDULE_ACTION_TYPE_KEY = "type"
SCHEDULE_ACTION_PARAMETERS_KEY = "parameters"
ACTION_TYPE_RUN_PIPELINE = "run_pipeline"
PIPELINE_NAME_KEY_IN_SCHEDULES = "pipeline_name"
PIPELINE_VERSION_KEY_IN_SCHEDULES = "pipeline_version"
RUN_PARAMETERS_KEY_IN_SCHEDULES = "run_parameters"
ACTIVE_SCHEDULE_FLAG = "active"
INACTIVE_SCHEDULE_FLAG = "inactive"
RECURRING_FREQUENCY_DAILY = "daily"
RECURRING_FREQUENCY_WEEKLY = "weekly"
RECURRING_FREQUENCY_MONTHLY = "monthly"
RECURRING_FREQUENCY_YEARLY = "yearly"
SCHEDULING_DOCKER_IMAGE = "dockerregistry.xpresso.ai/library/" \
                          "xpresso_scheduling:1.1"
SCHEDULING_ENV_PARAM_PROJECT_NAME = "PROJECT_NAME"
SCHEDULING_ENV_PARAM_SCHEDULE_NAME = "SCHEDULE_NAME"
SCHEDULING_ENV_PARAM_MASTER_NODE = "MASTER_NODE"
SCHEDULING_CRONJOB_PREFIX = "xs--"
SCHEDULING_NAMESPACE = "xpresso-schedules"

# Jupyterhub
CONFIG_JUPYTERHUB = "jupyterhub"

# Remote NFS server constants
REMOTE_NFS_CONFIG = "remote_nfs_server_config"
REMOTE_NFS_SERVER = "server_ip"
REMOTE_NFS_USERNAME = "server_login"
REMOTE_NFS_PASSWORD = "server_password"
REMOTE_NFS_MOUNT_PATH = "mount_location"


# project manager constants

PROJECT_COMMANDS = ["create_project", "modify_project",
                    "get_projects", "deactivate_project",
                    "find_project"]

# build manager constants

BUILD_COMMANDS = ["build_project", "get_build_version", "coderepo_branch"]

# deployment manager constants
DEPLOY_COMMANDS = ["get_pipeline_versions", "deploy_project",
                   "undeploy_project", "cleanup_project"]

# versioning commands
VERSIONING_COMMANDS = ["versioning_auth", "create_repo", "list_repos", "list_branches",
                       "list_commits", "list_datasets", "create_branch",
                       "pull_dataset", "push_dataset", "delete_branch"]

# cluster management commands
CLUSTER_COMMANDS = ["register_cluster", "get_cluster", "deactivate_cluster"]

# experiment commands
EXPERIMENT_COMMANDS = ["compare_experiments", "pipelines_list",
                       "get_experiment_parameters", "store_experiment_parameters",
                       "start_experiment", "terminate_experiment", "restart_experiment",
                       "pause_experiment"]

# schedules commands
SCHEDULES_COMMANDS = ["delete_schedule", "create_schedule", "get_schedule",
                      "modify_schedule", "stop_schedule", "restart_schedule"]
# run commands
RUN_INFO_COMMANDS = ["get_run_info", "update_run_info", "find_experiment"]

# user manager commands
USER_MANAGER_COMMANDS = ["register_user", "get_users", "modify_user",
                         "deactivate_user", "update_password",
                         "get_user_details"]

# SSO commnads
SSO_COMMANDS = ["validate_token", "get_auth_url",
                "sso_login", "sso_auth"]

# node manager commands
NODE_MANAGER_COMMANDS = ["deactivate_node", "provision_node", "get_nodes",
                         "register_node", "assign_node"]

# xpresso info manager commands
XPRESSO_GENERIC_REQUEST_COMMANDS = ["get_version", "get_info", "update_xpresso",
                                    "ssl_cert_presto", "update_xpresso_package"]

# file explorer commands
FILE_EXPLORER_COMMANDS = ["fetch_project_files", "delete_files", "upload_files",
                          "download_files", "create_folder"]

# DV DB handler commands
DV_DB_COMMANDS = ["dv_db_get_branch", "dv_db_delete_branch",
                  "dv_db_update_branch", "dv_db_create_branch"]

# project metric command
PROJECT_METRIC_COMMANDS = ["get_kpis", "get_plots"]

# authentication commands
AUTH_COMMANDS = ["login", "logout"]

# Kubernetes helper API commands
DEPLOY_API_COMMANDS = ["deployed_component_log", "component_deployment_details",
                       "component_deployment_history", "list_project_deployment"]

# component base image commands
BASE_IMAGE_COMMANDS = ["add_new_image", "update_image_info", "get_image_info"]

# Experiment Info API commands
EXPERIMENT_INFO_API_COMMANDS = ["experiment_run_details", "experiment_history",
                                "experiment_list"]

# Build Info API commands
BUILD_INFO_API_COMMANDS = ["build_list", "build_history", "build_details"]

# Project Metrics Constants
PROJECT_NAME_KEY_METRICS = "project_name"
PERSONALIZATION_KEY_METRICS = "personalization"
METRICS_KEY_METRICS = "metrics"
KPIS_KEY_METRICS = "kpis"
PLOTS_KEY_METRICS = "plots"
PLOT_NAMES_KEY_METRICS = "plot_names"
TOTAL_DEVELOPERS_METRIC = "total_developers"
CREATED_ON_METRIC = CREATED_ON_KEY
TOTAL_COMPONENTS_METRIC = "total_components"
TOTAL_DATABASE_METRIC = "total_databases"
TOTAL_JOBS_METRIC = "total_jobs"
TOTAL_SERVICES_METRIC = "total_services"
TOTAL_PIPELINE_JOBS_METRIC = "total_pipeline_jobs"
TOTAL_INF_SVCS_METRIC = "total_inference_services"
TOTAL_PYSPARK_JOBS_METRIC = "total_pyspark_jobs"
TOTAL_PIPELINES_METRIC = "total_pipelines"
TOTAL_PYSPARK_PIPELINES_METRIC = "total_pyspark_pipelines"
TOTAL_SVC_MESHES_METRIC = "total_svc_meshes"
TOTAL_BUILDS_METRIC = "total_builds"
LAST_BUILT_COMPONENT_METRIC = "last_built_component"
TOTAL_DEPLOYMENTS_METRIC = "total_deployments"
LAST_DEPLOYED_COMPONENT_METRIC = "last_deployed_component"
TOTAL_MESHES_DEPLOYED_METRIC = "total_svc_mesh_deployed"
LAST_DEPLOYED_MESH_METRIC = "last_deployed_svc_mesh"
TOTAL_CODEREPO_COMMITS_METRIC = "total_coderepo_commits"
TOTAL_CODEREPO_MASTER_COMMITS_METRIC = "total_coderepo_commits_on_master_branch"
TOTAL_CODEREPO_MERGED_PR_METRIC = "total_coderepo_merged_pr"
LAST_MONTH_TOTAL_CODEREPO_COMMITS_BY_USER_METRIC = "last_month_total_coderepo_commits"
LAST_MONTH_TOTAL_CODEREPO_MASTER_COMMITS_BY_USER_METRIC = "last_month_total_coderepo_master_commits"
LAST_MONTH_TOTAL_CODEREPO_MERGED_PR_BY_AUTHOR_METRIC = "last_month_total_coderepo_merged_prs_per_author"
TOTAL_DATAREPO_BRANCHES_METRIC = "total_datarepo_branches"
TOTAL_DATAREPO_COMMITS_METRIC = "total_datarepo_commits"
LAST_UPDATED_DATAREPO_BRANCH_METRIC = "last_updated_datarepo_branch"
LAST_UPDATED_DATAREPO_COMMIT_METRIC = "last_updated_datarepo_commit"
TOTAL_EXPERIMENTS_RAN_METRIC = "total_experiments_ran"
TOTAL_PIPELINES_DEPLOYED_METRIC = "total_pipelines_deployed"
LAST_PIPELINE_RUN_METRIC = "last_pipeline_run"
LAST_EXPERIMENT_RUN_METRIC = "last_experiment_run"
LAST_SUCCESSFUL_EXPERIMENT_RUN_METRIC = "last_successful_experiment_run"
LAST_EXPERIMENT_DATE_METRIC = "last_experiment_date"
PROJECT_METRIC_SECTION = "project"
BUILDS_METRIC_SECTION = "builds"
DEPLOYMENTS_METRIC_SECTION = "deployments"
CODEREPO_METRIC_SECTION = "coderepo"
DATAREPO_METRIC_SECTION = "datarepo"
EXPERIMENTS_METRIC_SECTION = "experiments"
EXPERIMENT_DISTRIBUTION_METRIC = "experiment_distribution"
COMPONENT_DISTRIBUTION_METRIC = "component_distribution"
PIPELINE_RUNS_DISTRIBUTION_METRIC = "pipelines_runs_distribution"
ENV_DEPLOYMENT_DISTRIBUTION_METRIC = "environment_deployment_distribution"
LAST_MONTH_DATAREPO_COMMITS_METRIC = "last_month_datarepo_commits"
LAST_MONTH_EXPERIMENTS_METRIC = "last_month_experiments"
LAST_MONTH_DATAREPO_COMMIT_BY_USER_METRIC = "last_month_datarepo_commit_by_user"
LAST_MONTH_EXPERIMENTS_BY_USER_METRIC = "last_month_experiments_by_user"
PLOT_METRICS_TITLE_KEY = "title"
PLOT_METRICS_CATEGORY_KEY = "category"
PLOT_METRICS_PLOT_TYPE_KEY = "plot_type"
PLOT_METRICS_Y_TITLE_KEY = "y_title"
PLOT_METRICS_X_TITLE_KEY = "x_title"
PLOT_METRICS_DATA_KEY = "data"
PLOT_METRICS_DATE_KEY = "date"
PLOT_METRICS_COUNT_KEY = "count"
BIGDATA_ENVIRONMENT_KEY_METRICS = "bigdata"
GENERAL_ENVIRONMENT_KEY_METRICS = "general"
PLOT_TYPE_BAR_CHART = "bar_chart"
PLOT_TYPE_PIE_CHART = "pie_chart"
PLOT_TYPE_LINE_CHART = "line_chart"
CODEREPO_MASTER_BRANCH = "master"
CODEREPO_PR_MERGED_STATE = "MERGED"
CODEREPO_RESPONSE_DATE_KEY = "date"
CODEREPO_RESPONSE_CREATED_ON_KEY = "created_on"
CODEREPO_RESPONSE_AUTHOR_KEY = "author"
CODEREPO_RESPONSE_USER_KEY = "user"
CODEREPO_RESPONSE_DISPLAY_NAME_KEY = "display_name"
CODEREPO_RESPONSE_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S%z'

# INSTANCE_POLICY_CONSTANTS
BUILD_POLICY_KEY = "build_policy"
DEPLOYMENT_POLICY_KEY = "deployment_policy"

# Vault Constants
VAULT_CONFIG = "vault"
VAULT_IP_CONFIG = "server_ip"
VAULT_PORT_CONFIG = "server_port"
VAULT_TOKEN_CONFIG = "token"
VAULT_TOKEN_HEADER = "X-Vault-Token"
VAULT_NAMESPACE_XPRESSO = "xpresso"
VAULT_IP_SUFFIX_KV = "/v1/kv/"
VAULT_IP_SUFFIX_XPRESSO = f"{VAULT_IP_SUFFIX_KV}{VAULT_NAMESPACE_XPRESSO}"
VAULT_RESPONSE_DATA_KEY = "data"
USE_VAULT_PASSWORDS_ENV = "USE_VAULT_PASSWORDS"
VAULT_PASSWORDS_CACHE_KEY = "vault_passwords"

# base image collection map
mongo_collection_auto_id_map = {
    BASE_IMAGES_COLLECTION: "id"
}
